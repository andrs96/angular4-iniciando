import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[primeiraDiretiva]'
})
export class PrimeiraDiretivaDirective {

  constructor(private el: ElementRef) {
    this.el.nativeElement.innerHTML += 'conteudo inserido';
  }

  @Input()
  myTask: string;

  @HostListener('click')
  onClick() {
    alert(this.myTask);
  }
}
