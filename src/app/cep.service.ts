import { Injectable } from '@angular/core';
import { Http} from "@angular/http";
import {Cep} from "./Cep";
// import 'rxjs/add/operator/toPromise';

@Injectable()
export class CepService {

  constructor(private http:Http) { }

  buscarCep(cep:string) {
    return this.http.get(`https://viacep.com.br/ws/${cep}/json/`)
      .toPromise()
      /*
        Estou recebendo a resposta, convertendo para objeto, hidratando no metodo converteRespostaParaCep
        que me retorna um objeto de Cep e retornando como uma promessa.
       */
      .then(response => this.converteRespostaParaCep(response.json()));
  }

  public converteRespostaParaCep(cepResposta):Cep{
    let cep = new Cep();
    cep.cep = cepResposta.cep;
    cep.logradouro = cepResposta.logradouro;
    cep.complemento = cepResposta.complemento;
    cep.numero = cepResposta.numero;
    cep.cidade = cepResposta.localidade;
    cep.uf = cepResposta.uf;
    cep.bairro = cepResposta.bairro;
    return cep;
  }
}
