import { Component } from '@angular/core';
import { CepService } from "../cep.service";
import { Cep } from "../Cep";

@Component({
  selector: 'cep',
  templateUrl: './cep.component.html',
  styleUrls: ['./cep.component.css']
})
export class CepComponent {
  cepObj = new Cep();
  btnCarregando = false;

  constructor(private cepService:CepService) {
    this.cepObj.cep = '01001-000';
  }

  buscar() {
    this.btnCarregando = true;

    this.cepService.buscarCep(this.cepObj.cep)
      .then((cep:Cep) => {
        this.cepObj = cep;
        this.btnCarregando = false;
      })
      .catch(() => {
        let cep = new Cep();
        cep.cep = this.cepObj.cep;
        this.cepObj = cep;
        alert('Não foi possível fazer a busca!');
        this.btnCarregando = false;
      });
  }
}
