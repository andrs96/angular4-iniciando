import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatCurrency'
})
export class FormatCurrencyPipe implements PipeTransform {

  transform(valor: any, local = 'pt-BR'): any {
    return new Intl.NumberFormat(local, {style: 'currency', currency: 'BRL'}).format(valor);
  }

}
