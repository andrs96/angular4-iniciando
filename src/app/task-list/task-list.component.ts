import { Component } from '@angular/core';
import { Task } from "../../Task"
import { TaskService } from "../task.service";

@Component({
    selector: 'task-list',
    templateUrl: './task-list.component.html',
    styleUrls: ['./task-list.component.css']
})
export class TaskListComponent {
    tasks:Array<Task>;

    constructor(private taskService:TaskService){
        // this.taskService.tasks.push(
        //     {name: 'Cozinha', value: 50, date: '2017/07/07'}
        // );

        this.tasks = this.taskService.tasks;
    }
}
