import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from './app.component';
import { TaskListComponent } from './task-list/task-list.component';
import { FormatCurrencyPipe } from './format-currency.pipe';
import { FormatDatePipe } from './format-date.pipe';
import { HttpModule} from "@angular/http";
import { PrimeiraDiretivaDirective } from './primeira-diretiva.directive';
import { TaskNewComponent } from './task-new/task-new.component';
import { TaskService } from "./task.service";
import { CepComponent } from './cep/cep.component';
import { CepService } from "./cep.service";

const appRoutes:Routes = [
  {path: 'new', component: TaskNewComponent},
  {path: 'list', component: TaskListComponent},
  {path: 'cep', component: CepComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    TaskListComponent,
    FormatCurrencyPipe,
    FormatDatePipe,
    PrimeiraDiretivaDirective,
    TaskNewComponent,
    CepComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [TaskService, CepService],
  bootstrap: [AppComponent]
})
export class AppModule { }
